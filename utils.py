import cv2
import matplotlib.pyplot as plt
import torch
from torch.utils.data import Dataset
from tqdm import tqdm
import numpy as np
from PIL import Image
import matplotlib.pyplot as plt
from numpy.fft import fft2, ifft2
from skimage import color, data, restoration
import scipy
from scipy.signal import convolve2d


def plot_dataset_exemple():
    blur_exemple = cv2.imread(f"ai_deep_learning_course/input/gaussian_blurred/24_HUAWEI-Y9_S.jpg")
    sharp_exemple = cv2.imread(f"ai_deep_learning_course/input/sharp/24_HUAWEI-Y9_S.jpg")
    plt.figure(figsize = (10,10))
    plt.subplot(1, 2, 1)
    plt.imshow(blur_exemple)
    plt.subplot(1, 2, 2)
    plt.imshow(sharp_exemple)
    plt.show()


def validate(model, dataloader, val_data, device, loss_func):
    model.eval()
    running_loss = 0.0
    with torch.no_grad():
        for i, data in tqdm(enumerate(dataloader), total=int(len(val_data)/dataloader.batch_size)):
            blur_image = data[0]
            sharp_image = data[1]
            blur_image = blur_image.to(device)
            sharp_image = sharp_image.to(device)
            outputs = model(blur_image)
            loss = loss_func(outputs, sharp_image)
            running_loss += loss.item()
        val_loss = running_loss/len(dataloader.dataset)
        print(f"Val Loss: {val_loss:.5f}")
        
        return val_loss

class DeblurDataset(Dataset):
    def __init__(self, blur_paths, sharp_paths=None, transforms=None):
        self.X = blur_paths
        self.y = sharp_paths
        self.transforms = transforms
         
    def __len__(self):
        return (len(self.X))
    
    def __getitem__(self, i):
        blur_image = cv2.imread(f"ai_deep_learning_course/input/gaussian_blurred/{self.X[i]}")
        
        if self.transforms:
            blur_image = self.transforms(blur_image)
            
        if self.y is not None:
            sharp_image = cv2.imread(f"ai_deep_learning_course/input/sharp/{self.y[i]}")
            sharp_image = self.transforms(sharp_image)
            return (blur_image, sharp_image)
        else:
            return blur_image